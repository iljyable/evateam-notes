import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoteComponent } from './components/note/note.component';
import { CreateNoteComponent } from './components/create-note/create-note.component';
import { NotFoundComponent } from './components/not-found/not-found.component';


const routes: Routes = [
  { path: 'note/:id', component: NoteComponent },
  { path: 'create', component: CreateNoteComponent },
  { path: '', redirectTo: '/note/0', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
