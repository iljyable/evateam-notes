import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { NotesService } from '../../services/notes.service';

import { Note } from '../../models/note.model';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {
  note$: Observable<Note>;

  constructor(private route: ActivatedRoute, private notesService: NotesService) { }

  ngOnInit(): void {
    this.note$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        let id = Number(params.get('id'));

        return this.notesService.get(id);
      }),
    );
  }

}
