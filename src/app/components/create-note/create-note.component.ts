import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { NotesService } from '../../services/notes.service';

import { Note } from '../../models/note.model';

@Component({
  selector: 'app-create-note',
  templateUrl: './create-note.component.html',
  styleUrls: ['./create-note.component.css']
})
export class CreateNoteComponent implements OnInit {
  form = new FormGroup({
    title: new FormControl('', Validators.required),
    content: new FormControl('', Validators.required)
  });

  constructor(private notesService: NotesService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    let id = this.notesService.generateId();
    let title = this.form.get('title').value;
    let content = this.form.get('content').value;
    let createdAt =  Math.floor(Date.now() / 1000);

    let note: Note = {
      id,
      title,
      content,
      createdAt
    };

    this.notesService.add(note);

    this.router.navigate(['/note', id]);
  }
}
