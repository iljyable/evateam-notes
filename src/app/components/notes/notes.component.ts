import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { NotesService } from '../../services/notes.service'

import { Note } from '../../models/note.model';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
  notes$: Observable<Note[]>;

  constructor(private notesService: NotesService) { }

  ngOnInit(): void {
    this.notes$ = this.notesService.notes$;
  }

}
