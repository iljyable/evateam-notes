import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject, of, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { Note } from '../models/note.model';

@Injectable({
  providedIn: 'root'
})
export class NotesService {
  notes$ = new BehaviorSubject<Note[]>([]);

  constructor() { }

  add(note: Note) {
    let notes = this.notes$.getValue();

    notes.push(note);

    this.notes$.next(notes);

    return notes;
  }

  get(id: number): Observable<Note> {
    return this.notes$.pipe(
      switchMap((notes: Note[]) => {
        let note = notes.find(n => n.id == id);

        if (!note) {
          return throwError(Error("Note wasn't found."))
        }

        return of(note);
      })
    );
  }

  generateId(min: number = 0, max: number = Number.MAX_SAFE_INTEGER): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    let id = Math.floor(Math.random() * (max - min + 1)) + min;

    let note = this.notes$.getValue().find(n => n.id == id);

    if (note) {
      id = this.generateId(min, max);
    }

    return id;
  }

}
