import { Injectable, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { tap, takeUntil } from 'rxjs/operators';

import { NotesService } from './notes.service';

import { Note } from '../models/note.model';

const NOTES: Note[] = [];

for (let i = 0; i < 5; i++) {
  let date = new Date()
  date = new Date(date.setHours(date.getHours() - 5 - i))

  let note: Note = {
    id: i,
    title: `Заметка ${i + 1}`,
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    createdAt: Math.floor(date.getTime() / 1000)
  }

  NOTES.push(note);
}

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService implements OnDestroy {
  unsubscribe$ = new Subject<void>();

  constructor(private notesService: NotesService) {
    let notes: Note[] = JSON.parse(localStorage.getItem('notes'));

    if (!notes) {
      notes = NOTES;
    }

    this.notesService.notes$.next(notes);

    this.notesService.notes$.pipe(
      tap((notes: Note[]) => {
        localStorage.setItem('notes', JSON.stringify(notes));
      }),
      takeUntil(this.unsubscribe$)
    ).subscribe()
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
